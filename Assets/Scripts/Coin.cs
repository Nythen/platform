﻿using UnityEngine;

public class Coin : MonoBehaviour
{
    private CoinHandler coinHandler;

    private void Awake()
    {
        coinHandler = GameObject.Find("CoinHandler").GetComponent<CoinHandler>();
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            //Add coin
            coinHandler.AddCoin();
            Destroy(gameObject);
        }
    }
}
