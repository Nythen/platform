﻿using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;
using System.Collections;

public class CoinHandler : MonoBehaviour
{
    [SerializeField] private Text coinText;
    [HideInInspector] public int coinAmount;
    private int savedCoins;

    private void Awake()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (savedCoins < coinAmount)
        {
            coinAmount = savedCoins; //Remove extra coins on death
        }

        coinText.text = coinAmount.ToString("D2");
    }

    public void AddCoin()
    {
        coinAmount++;
        coinText.text = coinAmount.ToString("D2");
        SaveCoins();
        SoundManager.PlaySound(SoundManager.soundList.coin);
    }

    public void AddCoinsAmount(int amount)
    {
        StartCoroutine(ChangeCoinsSmoothly(amount, false));
        /*coinAmount += amount;
        coinText.text = coinAmount.ToString("D2");*/
    }

    public void SpendCoins(int amount)
    {
        StartCoroutine(ChangeCoinsSmoothly(amount, true));
        /*coinAmount -= amount;
        coinText.text = coinAmount.ToString("D2");*/
    }

    public void SaveCoins()
    {
        savedCoins = coinAmount;
    }

    IEnumerator ChangeCoinsSmoothly(int amount, bool isSpending)
    {
        float timeToShow = 1f / amount;

        if (isSpending)
        {
            int totalCoins = coinAmount - amount;
            while (coinAmount != totalCoins)
            {
                coinAmount--;

                coinText.text = coinAmount.ToString("D2");
                SoundManager.PlaySound(SoundManager.soundList.coin);
                yield return new WaitForSeconds(timeToShow);
            }
        }
        else
        {
            int totalCoins = coinAmount + amount;
            while (coinAmount != totalCoins)
            {
                coinAmount++;

                coinText.text = coinAmount.ToString("D2");
                SoundManager.PlaySound(SoundManager.soundList.coin);
                yield return new WaitForSeconds(timeToShow);
            }
        }

        SaveCoins();
    }

    public void NotEnoughCoins()
    {
        StartCoroutine(NotEnoughCoinsVisual());
    }

    IEnumerator NotEnoughCoinsVisual()
    {
        SoundManager.PlaySound(SoundManager.soundList.notEnoughCoins);

        LeanTween.scale(coinText.gameObject, Vector3.one * 1.5f, 0.15f);
        yield return new WaitForSeconds(0.15f);

        coinText.color = Color.red;
        yield return new WaitForSeconds(0.1f);

        LeanTween.scale(coinText.gameObject, Vector3.one, 0.15f);
        yield return new WaitForSeconds(0.15f);

        coinText.color = Color.black;
    }
}
