﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Congratulations : MonoBehaviour
{
    [SerializeField] GameObject congratsChest;
    [SerializeField] GameObject endGameUI;

    private void Awake()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (GameComplete())
        {
            congratsChest.SetActive(true);
        }
    }

    private bool GameComplete()
    {
        if (LevelManager.Instance.isFirstLevelCompleted == true &&
            LevelManager.Instance.isSecondLevelCompleted == true &&
            LevelManager.Instance.isThirdLevelCompleted == true)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void EndGame()
    {
        PlayerController playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        playerController.FreezePlayer();

        endGameUI.SetActive(true);
    }
}
