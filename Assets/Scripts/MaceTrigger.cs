﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaceTrigger : MonoBehaviour
{
    [SerializeField] Mace mace;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            mace.Drop();
        }
    }
}
