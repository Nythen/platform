﻿using UnityEngine;

public class CamaraFollow : MonoBehaviour
{
    
    [SerializeField] private Transform target;

    [SerializeField] [Range(0.05f, 0.5f)]
    private float smoothSpeed;

    [SerializeField] private Vector3 offset;

    private Vector3 velocity = Vector3.zero;

    private void LateUpdate()
    {
        Vector3 desiredPosition = target.position + offset;
        transform.position = Vector3.SmoothDamp(transform.position, desiredPosition, ref velocity, smoothSpeed);
    }
}
