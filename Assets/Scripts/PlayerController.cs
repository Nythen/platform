﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    public bool isSprinting;
    public bool isInteracting;

    [SerializeField] private float speed, sprintMultiplier, jumpSpeed, doubleJumpMultiplier;
    [SerializeField] private LayerMask ground;

    private bool isDead;
    private bool canDoubleJump;

    private PlayerActionController playerActionController;
    private Rigidbody2D rb;
    private Collider2D col;
    private Animator animator;
    private SpriteRenderer spriteRenderer;

    void Awake()
    {
        playerActionController = new PlayerActionController();
        rb = GetComponent<Rigidbody2D>();
        col = GetComponent<Collider2D>();
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();

        isDead = false;
    }

    void OnEnable()
    {
        playerActionController.Enable();
        
    }

    void OnDisable()
    {
        playerActionController.Disable();
    }

    void Start()
    {
        playerActionController.Land.Jump.performed += _ => Jump();
        playerActionController.Land.Sprint.performed += ctx => Sprint(ctx);
        playerActionController.Land.Sprint.canceled += ctx => Sprint(ctx);
        playerActionController.Land.Interact.performed += ctx => Interact(ctx);
        playerActionController.Land.Interact.canceled += ctx => Interact(ctx);
        playerActionController.UI.Menu.performed += Menu_performed;
    }

    private void Menu_performed(InputAction.CallbackContext obj)
    {
        FindObjectOfType<PauseMenu>().EnablePauseContainer();
    }

    private void Interact(InputAction.CallbackContext context)
    {
        if (context.performed && !isInteracting)
        {
            isInteracting = true;
        }
        else if (context.canceled)
        {
            isInteracting = false;
        }
    }

    private void Sprint(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            isSprinting = true;
            speed *= sprintMultiplier;
        }
        else if (context.canceled)
        {
            isSprinting = false;
            speed /= sprintMultiplier;
        }
    }

    private void Jump()
    {
        if (IsGrounded())
        {
            rb.AddForce(new Vector2 (0, jumpSpeed), ForceMode2D.Impulse);
            SoundManager.PlaySound(SoundManager.soundList.jump);
        }
        else if(canDoubleJump)
        {
            rb.velocity = Vector2.zero;
            rb.AddForce(new Vector2(0, jumpSpeed * doubleJumpMultiplier), ForceMode2D.Impulse);
            canDoubleJump = false;
            SoundManager.PlaySound(SoundManager.soundList.jump);
        }
    }

    private bool IsGrounded()
    {
        Vector2 topLeftPoint = transform.position;
        topLeftPoint.x -= col.bounds.extents.x;
        topLeftPoint.y += col.bounds.extents.y;

        Vector2 bottomRightPoint = transform.position;
        bottomRightPoint.x += col.bounds.extents.x;
        bottomRightPoint.y -= col.bounds.extents.y;

        return Physics2D.OverlapArea(topLeftPoint, bottomRightPoint, ground);
    }

    void Update()
    {
        if (!isDead)
        {
            Move();
        }

        /*if (Input.GetKeyDown(KeyCode.K))
        {
            LevelManager.Instance.isFirstLevelCompleted = true;
            LevelManager.Instance.isSecondLevelCompleted = true;
            LevelManager.Instance.isThirdLevelCompleted = true;
        }*/
    }

    void FixedUpdate()
    {
        if (!isDead) 
        {
            if (!IsGrounded())
            {
                if (rb.velocity.y < 0) //Add gravity for more "pleasant" jump
                {
                    rb.velocity = new Vector2(0, rb.velocity.y * rb.gravityScale);
                }

                //Set the animator for the Jump-Fall Blend
                animator.SetFloat("yVelocity", rb.velocity.y);
                animator.SetBool("Jump", true);
            }
            else
            {
                canDoubleJump = true;
                animator.SetBool("Jump", false);
            }
        }
        else
        {
            this.enabled = false;
        }

    }

    private void Move()
    {
        //Read the movement value
        float movementInput = playerActionController.Land.Move.ReadValue<float>();

        //Move the player
        Vector3 currentPosition = transform.position;
        currentPosition.x += movementInput * speed * Time.deltaTime;
        transform.position = currentPosition;

        //Animation
        if (movementInput != 0)
        {
            animator.SetBool("Runing", true);
        }
        else
        {
            animator.SetBool("Runing", false);
        }

        //Sprite Flip
        if (movementInput == -1)
        {
            spriteRenderer.flipX = true;
        }
        else if(movementInput == 1)
        {
            spriteRenderer.flipX = false;
        }
    }

    public void Die()
    {
        animator.SetTrigger("Die");
        //Lock movement
        isDead = true;

        //Pause/death Menu
        PauseMenu pauseMenu = FindObjectOfType<PauseMenu>();
        pauseMenu.EnablePauseContainer();
    }

    public void FreezePlayer()
    {
        isDead = true;
    }
}
