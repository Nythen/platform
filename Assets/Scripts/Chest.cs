﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Chest : MonoBehaviour
{
    public enum ChestEndLevel {None, One, Two, Three, Game};
    public ChestEndLevel chestEndLevel;

    public GameObject coinsParticle;
    public int coinsToDrop = 10;

    private Animator anim;
    private PlayerController playerController;
    private CoinHandler coinHandler;

    private bool isChestOpen = false;

    private void Start()
    {
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        coinHandler = GameObject.Find("CoinHandler").GetComponent<CoinHandler>();
        anim = GetComponent<Animator>();
    }

    private void OnTriggerStay2D(Collider2D col)
    {
        if (col.CompareTag("Player"))
        {
            if (playerController.isInteracting && !isChestOpen)
            {
                anim.SetTrigger("Open");
                isChestOpen = true;
                //Instantiate coins
                ChestCoins();
                //Save if the level is completed
                ChestToEndLevel();
            }
        }
    }

    private void ChestCoins()
    {
        Vector3 position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y - 0.5f, transform.position.z);

        GameObject coinParticle = Instantiate(coinsParticle, position, Quaternion.identity);

        //Add Coins
        coinHandler.AddCoinsAmount(coinsToDrop);

        Destroy(coinParticle, 4f);
    }

    IEnumerator ReturnToLevelSelect()
    {
        yield return new WaitForSeconds(3f);
        SceneManager.LoadScene((int)SceneIndexes.LEVEL_SELECT);
    }

    void ChestToEndLevel()
    {
        switch (chestEndLevel)
        {
            case ChestEndLevel.None:
                break;

            case ChestEndLevel.One:
                LevelManager.Instance.isFirstLevelCompleted = true;
                //coinHandler.SaveCoins();
                //Coroutine to return to level Select
                StartCoroutine(ReturnToLevelSelect());
                break;

            case ChestEndLevel.Two:
                LevelManager.Instance.isSecondLevelCompleted = true;
                //coinHandler.SaveCoins();
                //Coroutine to return to level Select
                StartCoroutine(ReturnToLevelSelect());
                break;

            case ChestEndLevel.Three:
                LevelManager.Instance.isThirdLevelCompleted = true;
                //coinHandler.SaveCoins();
                //Coroutine to return to level Select
                StartCoroutine(ReturnToLevelSelect());
                break;

            case ChestEndLevel.Game:
                Congratulations congrats = FindObjectOfType<Congratulations>();
                congrats.EndGame();
                break;
        }
    }
}
