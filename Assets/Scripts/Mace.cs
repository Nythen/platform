﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mace : Enemy
{
    public float speed = 2;

    private Rigidbody2D rb;
    private Vector3 startPos;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.gravityScale = 0;
    }

    public void Drop()
    {
        StopAllCoroutines();
        StartCoroutine(RecoverPositionOnTime());
        rb.gravityScale = 1;
    }

    IEnumerator RecoverPositionOnTime()
    {
        if (startPos == Vector3.zero)
        {
            startPos = transform.position;
        }

        yield return new WaitForSeconds(.9f);
        SoundManager.PlaySound3D(SoundManager.soundList.stomp, transform.position);

        yield return new WaitForSeconds(4f);

        rb.gravityScale = 0;

        //Recover
        while (Vector3.Distance(transform.position, startPos) > 0.2f)
        {
            transform.position = Vector3.Lerp(transform.position, startPos, 0.1f);
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.identity, 0.1f);
            yield return new WaitForSeconds(0.05f);
        }

        transform.position = startPos;
        transform.rotation = Quaternion.identity;
    }
    
}
