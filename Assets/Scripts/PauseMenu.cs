﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;

public class PauseMenu : MonoBehaviour
{
    public GameObject pauseContainer;

    public void EnablePauseContainer()
    {
        pauseContainer.SetActive(true);
    }

    public void RetryLevel()
    {
        SoundManager.PlaySound(SoundManager.soundList.button);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ExitGame()
    {
        SoundManager.PlaySound(SoundManager.soundList.button);

        #if UNITY_EDITOR
        EditorApplication.isPlaying = false;
        #else
                    Application.Quit();
        #endif
    }


}
