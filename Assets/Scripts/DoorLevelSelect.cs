﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class DoorLevelSelect : MonoBehaviour
{
    public enum Levels {First, Second, Third};
    public Levels doorLevel;

    public GameObject blockSprites;

    private CoinHandler coinHandler;

    private bool isBlocked = true;
    private PlayerController playerController;

    private void Start()
    {
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        coinHandler = FindObjectOfType<CoinHandler>();

        CheckBlockedDoors();
    }

    void CheckBlockedDoors()
    {
        switch (doorLevel)
        {
            case Levels.First:
                isBlocked = false; //an exeption for Door 1
                break;
            case Levels.Second:
                if (LevelManager.Instance.isSecondLevelCompleted == true)
                {
                    UnlockDoor();
                }
                break;
            case Levels.Third:
                if (LevelManager.Instance.isThirdLevelCompleted == true)
                {
                    UnlockDoor();
                }
                break;
        }
    }

    private void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            if (playerController.isInteracting)
            {
                if (isBlocked) 
                {
                    if (coinHandler.coinAmount >= 25) //check coins to buy door
                    {
                        UnlockDoor();
                        coinHandler.SpendCoins(25);
                        SoundManager.PlaySound(SoundManager.soundList.unclockDoor);
                    }
                    else
                    {
                        Debug.Log("Not Enough coins to unclock the door -> " + coinHandler.coinAmount + " . Do Something to let the player know?");
                        coinHandler.NotEnoughCoins();
                    }
                }
                else
                {
                    SoundManager.PlaySound(SoundManager.soundList.unclockDoor);
                    //The Door isn't blocked so we'll load the level
                    LoadNextLevel();
                }
            }
        }
    }
    void LoadNextLevel()
    {
        coinHandler.SaveCoins();

        switch (doorLevel)
        {
            case Levels.First:
                SceneManager.LoadScene((int)SceneIndexes.LEVEL_1);
                break;
            case Levels.Second:
                SceneManager.LoadScene((int)SceneIndexes.LEVEL_2);
                break;
            case Levels.Third:
                SceneManager.LoadScene((int)SceneIndexes.LEVEL_3);
                break;
            default:
                Debug.LogError("This Door Level is incorrect -> " + doorLevel);
                break;
        }
    }

    void UnlockDoor()
    {
        isBlocked = false;
        blockSprites.SetActive(false);
    }
}
