using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioHandler : MonoBehaviour
{
    public static AudioHandler instance;
    
    public AudioSource musicSource;

    [Header("Music")]
    public AudioClip music;

    [Header("Sound Fx")]
    public ClipSound[] clipSoundsArray;


    private void Awake()
    {
        if (instance == null)
            instance = this;
    }

    [System.Serializable]
    public class ClipSound
    {
        public SoundManager.soundList sound;
        public AudioClip audioClip;
    }

    
    //Music - Maybe I should move to another script

    public void PlayMusic()
    {
        musicSource.clip = music;
        musicSource.Play();
        StartCoroutine(FadeMusic(true));
    }

    public void StopMusic()
    {
        StartCoroutine(FadeMusic(false));
    }

    IEnumerator FadeMusic(bool isFadeIn)
    {
        float timeToFade = 2f;
        float timeElapsed = 0f;

        if (isFadeIn)
        {
            while (timeElapsed < timeToFade)
            {
                musicSource.volume = Mathf.Lerp(0, 1, timeElapsed / timeToFade);
                timeElapsed += Time.deltaTime;
                yield return null;
            }            
        }
        else
        {
            while (timeElapsed < timeToFade)
            {
                musicSource.volume = Mathf.Lerp(1, 0, timeElapsed / timeToFade);
                timeElapsed += Time.deltaTime;
                yield return null;
            }

            musicSource.Stop();
        }
    }
}


