﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChainEnemy : Enemy
{
    [SerializeField] Transform middlePoint;

    private Rigidbody2D rb;
    private bool pushLeft = false;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        //Make a push every time x changes his sign to maintain the swinging
        if (transform.position.x > middlePoint.position.x && pushLeft == false)
        {
            rb.AddForce(new Vector2(2, 2), ForceMode2D.Impulse);
            pushLeft = true;
            SoundManager.PlaySound3D(SoundManager.soundList.chain, transform.position);
        }
        else if(transform.position.x < middlePoint.position.x && pushLeft == true)
        {
            rb.AddForce(new Vector2(-2, -2), ForceMode2D.Impulse);
            pushLeft = false;
            SoundManager.PlaySound3D(SoundManager.soundList.chain, transform.position);
        }
    }
}
