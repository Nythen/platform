﻿using UnityEngine;

public class LevelManager : MonoBehaviour
{
    #region SINGLETON PATTERN
    private static LevelManager _instance;
    public static LevelManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<LevelManager>();

                if (_instance == null)
                {
                    GameObject container = new GameObject("LevelManager");
                    _instance = container.AddComponent<LevelManager>();
                }
            }

            return _instance;
        }
    }
    #endregion

    [HideInInspector] public bool isFirstLevelCompleted = false;
    [HideInInspector] public bool isSecondLevelCompleted = false;
    [HideInInspector] public bool isThirdLevelCompleted = false;
}
