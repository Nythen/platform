﻿using UnityEngine;

public class SpawnHandler : MonoBehaviour
{
    [SerializeField] Transform spawn1, spawn2;

    LevelManager levelManager;
    PlayerController player;

    private void Awake()
    {
        levelManager = FindObjectOfType<LevelManager>();
        player = FindObjectOfType<PlayerController>();
    }

    private void Start()
    {
        if (levelManager.isFirstLevelCompleted && !levelManager.isSecondLevelCompleted)
        {
            player.transform.position = spawn1.position;
        }
        else if (levelManager.isSecondLevelCompleted && !levelManager.isThirdLevelCompleted)
        {
            player.transform.position = spawn2.position;
        }
        else if (levelManager.isThirdLevelCompleted)
        {
            player.transform.position = spawn1.position;
        }
    }
}
