﻿using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Door : MonoBehaviour
{
    public enum DoorType {Start, Exit };
    public DoorType doorType;

    [SerializeField] private GameObject doorInfo;

    private PlayerController playerController;

    private void Start()
    {
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            doorInfo.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            doorInfo.SetActive(false);
        }
    }
    private void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            if (playerController.isInteracting) //Integrate it on the Controller
            {
                //Open Door
                switch (doorType)
                {
                    case DoorType.Start:
                        StartDoor();
                        break;
                    case DoorType.Exit:
                        ExitDoor();
                        break;
                    default:
                        Debug.LogError("This Door type is incorrect -> " + doorType);
                        break;
                }
            }
        }
    }

    private void StartDoor()
    {
        SceneManager.LoadScene((int)SceneIndexes.LEVEL_SELECT);
    }

    private void ExitDoor()
    {
        #if UNITY_EDITOR
            EditorApplication.isPlaying = false;
        #else
            Application.Quit();
        #endif
    }
}
