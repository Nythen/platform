﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Saw : Enemy
{

    public Transform pinPoint;
    public Transform ponPoint;

    [SerializeField] float speed;

    bool towards = true;

    void Update()
    {
        Movement();
    }

    private void Movement()
    {
        //pinpon
        if (towards)
        {
            transform.position += new Vector3(1, 0, 0) * speed * Time.deltaTime;
            if (Vector3.Distance(transform.position, ponPoint.position) < 1.0f)
            {
                towards = false;
                SoundManager.PlaySound3D(SoundManager.soundList.saw, transform.position);
            }
        }
        else
        {
            transform.position += new Vector3(1, 0, 0) * -speed * Time.deltaTime;
            if (Vector3.Distance(transform.position, pinPoint.position) < 1.0f)
            {
                towards = true;
                SoundManager.PlaySound3D(SoundManager.soundList.saw, transform.position);
            }
        }

        //spin
        transform.Rotate(Vector3.forward * 500 * Time.deltaTime);
    }
}
